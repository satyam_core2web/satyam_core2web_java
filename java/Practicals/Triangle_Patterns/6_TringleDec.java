/*
6. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
3 3 3
2 2
1

Rows = 4
4 4 4 4
3 3 3
2 2
1
*/

import java.io.*;

class TringleDec{

        public static void main(String n[])throws IOException{

                BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

                System.out.print("Rows : ");
                int rows= Integer.parseInt(br.readLine());

		int num = rows;

                for(int i=1 ; i<=rows ; i++){

			for(int j=rows; j>=i ;j--){
			
				System.out.print(num +"   ");
			}

			num--;
			System.out.println("\n");
		}
	}
}
