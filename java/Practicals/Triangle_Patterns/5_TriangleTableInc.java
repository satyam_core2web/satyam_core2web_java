/*
5. WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
1
2 4
3 6 9

Rows = 4
1
2 4
3 6 9
4 8 12 16
*/

import java.io.*;

class IncTable{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.print("Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1 ; i<=rows ; i++){
		
			for(int j=1; j<=i ; j++){
			
				int num = i;

				System.out.print(num*j +"  " );
			}

			System.out.println("\n");
		}
	}
}
