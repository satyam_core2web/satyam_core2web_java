/*
8. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
1 2 3 4
2 3 4
3 4
4
Rows = 3
1 2 3
2 3
3
*/

import java.io.*;

class Pattern8{

	public static void main(String [] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1 ; i<=rows ; i++){
		
			int num = i;
			for(int j=rows ; j>=i ; j--){
			
				System.out.print(num++ +"   ");
			}
			System.out.println("\n");
		}
	}
}
