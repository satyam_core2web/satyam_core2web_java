/*
10. WAP in notebook & Dry run first then type
Take number of rows from user :
rows = 4

65 B 67 D
B 67 D
67 D
D
rows = 3
A 66 C
66 C
C
*/

import java.io.*;

class Mix10{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));

		System.out.print("Enter No. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows ; i++){
		
			int ch = 64+i;

			for(int j=rows ; j>=i ; j--){


				if(rows%2==0){
				
					if(ch%2==0){

                                        	System.out.print((char)ch+"  ");
                                	} else {

                                        	System.out.print(ch+"  ");
                                	}

				} else {
				
					if(ch%2==0){

                                       		System.out.print(ch+"  ");
                               	
				       	} else {

                                        	System.out.print((char) ch+"  ");
                                	}
				}
				ch++;
			}
			System.out.println("\n");
		}
	}
}



