/*
7. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1 2 3 4
1 2 3
1 2
1
Rows = 3

1 2 3
1 2
1
*/

import java.io.*;

class TrianglePattern{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 
		System.out.print("How Many Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1 ; i<=rows ; i++){
		
			int num = 1;
			for(int j=rows ; j>=i ; j--){
			
				System.out.print(num +"   ");
				num++;
			}
			System.out.println("\n");
		}
	}
}
