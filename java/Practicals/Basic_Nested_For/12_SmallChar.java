/*
Q8 Write a program to print the following pattern
Number of rows = 4
d c b a
d c b a
d c b a
d c b a
Number of rows = 3
c b a
c b a
c b a
*/


class SmallChar{

	public static void main(String n[]){
	

		for(int i=4; i>0; i--){
			
			char ch = 'd';	

			for (int j=4; j>0; j--){
			
				System.out.print(ch +"  ");
				ch--;
			}
			System.out.println();
		}
	}
}


