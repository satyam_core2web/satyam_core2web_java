/*
 Q7 Write a program to print the following pattern
Number of rows = 4
1A 2B 3C 4C
1A 2B 3C 4C
1A 2B 3C 4C
1A 2B 3C 4C
*/

class Both2{

	public static void main(String n[]){
	
		
		for(int i=1; i<=4; i++){

			char ch = 'A';
		
			for(int j=1; j<=4; j++){
			
				System.out.print(j + ""+ch++ +"  ");
			}
			System.out.println();
		}
	}
}
