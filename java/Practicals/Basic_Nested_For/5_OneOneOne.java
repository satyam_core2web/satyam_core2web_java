/*
Q4 Write a program to print the following pattern
Number of rows = 3
1 1 1
2 2 2
3 3 3
*/

class FourTime{

	public static void main(String n[]){
	
		int num = 1;
		for(int i=1 ; i<=3 ; i++){
		
			for(int j=1 ; j<=3 ;j++){
			
				System.out.print(num +"  ");
			}
			System.out.println();
			num++;
		}
	}
}
