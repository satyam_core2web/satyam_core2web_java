/*
Q8 Write a program to print the following pattern
Number of rows = 3
c b a
c b a
c b a
*/


class SmallChar2{

	public static void main(String n[]){
	

		for(int i=1; i<=3; i++){
			
			char ch = 'c';	

			for (int j=1; j<=3; j++){
			
				System.out.print(ch +"  ");
				ch--;
			}
			System.out.println();
		}
	}
}


