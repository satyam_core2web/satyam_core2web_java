/*
Q1 Write a program to print the following pattern.
Number of rows = 4
$# $# $# $#
$# $# $# $#
$# $# $# $#
$# $# $# $#
*/

class FourRow{

	public static void main(String n[]){
	
		for(int i = 1 ; i<=4 ; i++){
		
			for(int j=1 ; j<=4 ; j++){
			
				System.out.print("$#  ");
			}
			System.out.println();
		}
	}
}
