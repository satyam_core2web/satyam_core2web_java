/*
Q9 Write a program to print the following pattern

Number of rows = 4
D1 D2 D3 D4
D5 D6 D7 D8
D9 D10 D11 D12
D13 D14 D15 D16

*/

class Num4{

	public static void main(String n[]){
	
		int num = 1;
		for(int i=1; i<=4; i++){
		
			for(int j=1; j<=4; j++){
			
				System.out.print(" D" +num +"    ");
				num++;
			}
			System.out.println();
		}
	}
}
