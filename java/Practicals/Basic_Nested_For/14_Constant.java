/*
Q9 Write a program to print the following pattern
Number of rows = 3
C1 C2 C3
C4 C5 C6
C7 C8 C9
*/

class Num3{

	public static void main(String n[]){
	
		int num = 1;
		for(int i=1; i<=3; i++){
		
			for(int j=1; j<=3; j++){
			
				System.out.print(" C" +num +"  ");
				num++;
			}
			System.out.println();
		}
	}
}
