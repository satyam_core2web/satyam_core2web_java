/*
 Q7 Write a program to print the following pattern
Number of rows = 3
1A 2B 3C
1A 2B 3C
1A 2B 3C
*/

class Both{

	public static void main(String n[]){
	
		
		for(int i=1; i<=3; i++){

			char ch = 'A';
		
			for(int j=1; j<=3; j++){
			
				System.out.print(j + ""+ch++ +"  ");
			}
			System.out.println();
		}
	}
}
