/*
10.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    1
  2 1 2
3 2 1 2 3

row=4

         1
      2  1  2
   3  2  1  2  3
4  3  2  1  2  3  4
*/

import java.io.*;

class Pyramid10{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			for(int space=rows; space>i; space--){
			
				System.out.print(" \t");
			}

			int num = i;
			for(int j=1; j<=i*2-1; j++){
			
				if(j>i){
                                	
					if(num==0){
						num=2;
					}
					System.out.print(num++ +"\t");
				
				} else {
					System.out.print(num +"\t");
					num--;
				}
				//System.out.print(num +"\t");
			}
			System.out.println("\n");
		}

	}
}
