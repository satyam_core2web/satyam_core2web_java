/*
9.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    3
  4 3 2
5 4 3 2 1

row=4

      4
    5 4 3
  6 5 4 3 2
7 6 5 4 3 2 1
*/

import java.io.*;

class Pyramid9{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			for(int space=rows; space>i; space--){
			
				System.out.print(" \t");
			}

			int num = rows+i-1;
			for(int j=1; j<=i*2-1; j++){
			
				System.out.print(num + "\t");
				num--;
			}
			System.out.println("\n");
		}
	}
}
