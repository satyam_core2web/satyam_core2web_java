/*
2. WAP in notebook & Dry run first then type
Take number of rows from user :
row = 3
1 2 3
  3 4
    4
row =4
1 2 3 4
  4 5 6
    6 7
      7
      */

import java.io.*;

class NumTriangle{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("No. Of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = 1;
		for(int i=1; i<=rows; i++){
		
			for(int space=1; space<i; space++){
			
				System.out.print("   ");
			}

			for(int j=1; j<=rows-i+1; j++){
			
				System.out.print(num +"  ");
				num++;
			}
			num--;
			System.out.println("\n");
		}
		
	}
}
