/*
5. Write a program to print the countdown of days to submit the assignment

Input : day = 7
Output: 7 days remaining
6 days remaining
5 days remaining
.
.
1 days remaining
0 days Assignment is Overdue
*/

class CountDown{

	public static void main(String n[]){
	
		int day = 7;

		while(day >= 0){
		
			if(day > 0){
			
				System.out.println(day +" Days Remaining to Submit Your Assignmets.");
				
			} else {
			
				System.out.println(day +" Days Remaining, Your OVERDUE.");
			}
			day--;
		}
	}
}
