/*
 * 6. Write a program where you have to take input from the user for a character array and
print the characters.
*/

import java.util.*;

class CharacterArray{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Size of Array : ");
		int size = sc.nextInt();

		char ch[] = new char[size];

		System.out.println("Enter Characters in Array : ");

		for(int i=0;i<ch.length; i++){
		
			ch[i]=sc.next().charAt(0);
		}

		System.out.print("Elements :- ");
		
		for(int i=0;i<ch.length; i++){
		
			System.out.print(ch[i]+"\t");
		}
		System.out.println();
	}
}
