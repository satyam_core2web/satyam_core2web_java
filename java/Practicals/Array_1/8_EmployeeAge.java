/*
 * 8. Write a program where you have to store the employee’s age working at a company,
take count of employees from the user.Take input from the user.
*/

import java.util.*;

class EmpAge{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter No. of Employee Working in Company : ");
		int count = sc.nextInt();

		int age[]= new int[count];

		System.out.println("Enter age for Employee's : ");

		for(int i=0; i<age.length; i++){
		
			age[i]=sc.nextInt();
		}

		System.out.println("Age for Employee at ");
                for(int i=0; i<age.length; i++){    

			System.out.println(i +" is " +age[i]);
		
		}
		System.out.println("\n");
	}
}
