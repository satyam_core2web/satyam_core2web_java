/*
 *2. Take an input from the user where the size of the array should be 10 and print the
output of the user given elements of an array.
*/

import java.io.*;

class UserInputArray{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Size of array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");

		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+"\t");
		}
		System.out.println();
               
	}
}
