/*
3. WAP in notebook & Dry run first then type
Take number of rows from user :
row =3
9 4 5
36 7 8
81 10 11

row =4
16 5 6 7
64 9 10 11
144 13 14 15
256 17 18 19
*/

import java.io.*;

class SquarePattern3{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				if(j==1){
					System.out.print(num*num +"  ");
				} else {
				
					System.out.print(num +"  ");
				}
				num++;
			}
			System.out.println("\n");
		}
	}
}
