/*
9. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
2 6 6
3 4 9
2 6 6
row=4
2 6 6 12
3 4 9 8
2 6 6 12
3 4 9 8
*/

import java.io.*;

class SquarePattern9{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows :");
		int rows = Integer.parseInt(br.readLine());


		for(int i=1; i<=rows; i++){
	
			int num = 1;
		
			for(int j=1; j<=rows; j++){
			
				if(i%2==1){
				
					if(j%2==1){
					
						System.out.print(num*2 +"  ");
					} else {
					
						System.out.print(num*3 +"  ");
					}
				} else {
				
					if(j%2==1){
					
						System.out.print(num*3 +"  ");
					} else {
					
						System.out.print(num*2 +"  ");
					}
				}
				num++;
			}
			System.out.println("\n");
		}
	}
}
