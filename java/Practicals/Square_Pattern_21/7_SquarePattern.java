/*
 7. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
A 4 A
6 B 8
C 10 C
row=4
4 A 6 A
8 B 10 B
12 C 14 C
16 D 18 D
*/

import java.io.*;

class SquarePattern7{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;
		char ch = 'A';

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				if(num%2==1){
				
					System.out.print(ch +"  ");
				} else {
				
					System.out.print(num +"  ");
				}

				num++;
			}
			ch++;
			System.out.println(" \n ");
		}
	}
}
