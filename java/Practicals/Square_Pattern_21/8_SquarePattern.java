/*
 8. WAP in notebook & Dry run first then type
Take number of rows from user :

row=3
# C #
C # B
# C #
row=4
# D # C
D # C #
# D # C
D # C #
 */

import java.io.*;

class SquarePattern8{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());


		for(int i=1; i<=rows; i++){

			int ch = 64+rows;

			for(int j=1; j<=rows; j++){
			
				if(i%2==0){

					if(j%2==1){

                                                System.out.print((char) ch +"  ");
                                                ch--;
                                        } else {
	 
                                                System.out.print("#  ");
                                                
                                        }


				} else {
				
					if(j%2==1){

	                                        System.out.print("#  ");
        	                        } else {
	
        	                                System.out.print((char) ch +"  ");
						ch--;
                	                }

				}

	
			}
			System.out.println(" \n ");
		}
	}
}
