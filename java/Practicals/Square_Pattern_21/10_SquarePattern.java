/*
10. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
3 B 1
C B A
3 B 1
row=4
4 C 2 A
D C B A
4 C 2 A
D C B A
*/

import java.io.*;

class SquarePattern10{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Emnter no. of rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			int ch = 64+rows;
			int num = rows;

			for(int j=1; j<=rows; j++){
			
				if(i%2==1){
				
					if(j%2==1){
					
						System.out.print(num +"  ");
					} else {
					
						System.out.print((char)ch +"  ");
					}
				} else {
				
					System.out.print((char)ch +"  ");
				}
				ch--;
				num--;
			}
			System.out.println("\n");
		}
	}
}
