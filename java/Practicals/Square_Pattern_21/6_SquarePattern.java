/*
 6. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
9 4 25
6 49 8
81 10 121
row=4
4 25 6 49
8 81 10 121
12 169 14 225
16 289 18 361
*/

import java.io.*;

class SquarePattern6{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				if(num%2==1){
				
					System.out.print(num*num +"  ");
				} else {
				
					System.out.print(num +"  ");
				}

				num++;
			}
			System.out.println(" \n ");
		}
	}
}
