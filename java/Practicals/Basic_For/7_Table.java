/*
 7. Write a program to print a table of 17 up to 170.
Output: 17,34,51,68,...170
*/

class Table{

	public static void main (String n[]){
		
		int num = 17;
		System.out.println("Table of 17 -\n");
	
		for (int i = 1 ; i<=10 ; i++){
		
			System.out.println(num*i);
		} 
	}
}
