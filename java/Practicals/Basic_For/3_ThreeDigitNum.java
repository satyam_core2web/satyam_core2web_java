/*
3. Write a program to print the first ten 3-digit numbers.
Output:100,101,102,103,..,109
*/

class ThreeDigitNum{

	public static void main(String n[]){
	
		for (int i = 100 ; i<110 ; i++){
		
			System.out.println(i);
		}
	}
}
