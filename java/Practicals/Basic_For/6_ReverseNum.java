/*
6. Write a program to print the reverse from 80 - 10.
Output: 80,79,78,77,..10
*/

class Reverse{

	public static void main (String n[]){
	
		System.out.println("Reverse Numbers from 80 to 10 :-\n");

		for (int i = 80 ; i>=10 ; i--){
		
			System.out.println(i);
		}
	}
}
