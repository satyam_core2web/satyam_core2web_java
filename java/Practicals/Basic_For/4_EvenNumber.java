/*
4. Write a program to print the even numbers from 1 - 100.
Output:2,4,6,8,10,...100
*/


class EvenNum{

	public static void main(String n[]){
	
		System.out.println("Even Number 1-100 :- \n ");

		for (int i = 1 ; i<=100 ;i++){
		
			if(i%2==0){

				System.out.println(i);
			}
		}
	}
}
