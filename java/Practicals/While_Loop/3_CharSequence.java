/*
3. Write a program to print the character sequence given below when the
range given is
Input : start = 1 and end =6.
This means iterate the loop from 1 to 6
Output: A B C D E F
*/

class Char{

	public static void main(String n[]){
	
		int num = 1;
		char ch = 'A';

		while(num <= 6){
		
			System.out.print(ch++ +" ");
			num++;
		}
		System.out.println();

	} 
}
