/*
9. Write a program to count the odd digits and even digits in the given
number.
Input: 214367689
Output: Odd count : 4
Even count : 5
*/

class OddEven {

	public static void main(String n[]){
	
		int num = 214367689;
		int eCount = 0;
		int oCount = 0;

		while(num > 0){

			int rem = num%10;
			
			if(rem%2==0){

				eCount++;
				

			} else {
				
				oCount++;
				
			} 

			num/=10;
		
		}
		System.out.println("Count of Even Digits is - " +eCount);
		System.out.println("Count of Odd Digits is - " +oCount);
	}
}
