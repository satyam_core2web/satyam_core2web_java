/*
5.Dry run the given code in your notebook where you have to check is there any error if
yes mention so also if there is no error write the output.
*/

class ForIf{

	public static void main(String[] args){

		for(int i = 1;i>0;i++){

			if(i%2==0||i%3==0||i%4==0||i%5==0){

				continue;
			}

			if(i==100){
				break;
			}

			System.out.println(i+" ");

		}
	}
}
