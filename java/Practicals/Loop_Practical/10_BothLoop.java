/*
10.
Write a program where you have to use both for and while loop.
*/

class ForWhile{

	public static void main(String n[]){
	
		int num = 10;

		while(num>=1){

			for( ;num>=5; num--){

				System.out.println(num*num+"\n") ;
			
			}
			System.out.println("In While- " +num);
			num--;	
		}
	}
}
