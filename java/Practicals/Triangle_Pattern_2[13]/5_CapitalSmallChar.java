/*
5. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
A B C
a b
A

Rows = 4
A B C D
a b c
A B
a
*/

import java.io.*;

class CapitalSmallChar{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			char chCapital = 'A';
			char chSmall = 'a';
			for(int j=rows; j>=i; j--){
			
				if(i%2==1){
				
					System.out.print(chCapital +"  ");
					chCapital++;
				} else {
				
					System.out.print(chSmall +"  ");
					chSmall++;
				}
			}
			System.out.println("\n");
		}

	}
}
