/*
2. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
2 4 6 8
10 12 14
16 18
20
Rows = 5
2 4 6 8 10
12 14 16 18
20 22 24
26 28
30
*/

import java.io.*;

class EvenNum{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter on. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = 2;
		for(int i=1; i<=rows; i++){
		
			for(int j=rows; j>=i; j--){
			
				System.out.print(num +"   ");
				num+=2;
			}
			System.out.println("\n");
		}
	}
}
