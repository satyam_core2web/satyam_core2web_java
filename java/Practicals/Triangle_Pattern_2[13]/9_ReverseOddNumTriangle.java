/*
9. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
19 17 15 13
11 9 7
5 3:wq

1
Rows = 5
29 27 25 23 21
19 17 15 13
11 9 7
5 3
1
*/

import java.io.*;

class OddReverse{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int oddNum = rows*(rows+1)-1;

		for(int i=1; i<=rows; i++){
		
			for(int j=rows; j>=i; j--){
			
				System.out.print(oddNum +"  ");
				oddNum-=2;
			}
			System.out.println("\n");
		}
	}
}
