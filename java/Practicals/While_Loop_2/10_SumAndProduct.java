/*
1. 10.Write a program to print the sum of odd digits and product of even digits in a given number.

Input: 9367924
Output:
Sum of even digits: 28
Product of odd digits: 48
*/

class SumAndProduct{

	public static void main(String n[]){
	
		int num = 9367924;

		int sum = 0;
		int product = 1;

		while (num>0){
		
			int rem = num % 10;

			if(rem%2==0){
					
				product *=rem;
			} else {
			
				sum+=rem;
			}
			num/=10;
		}
		System.out.println("Sum Of Odd Digits in number = "+sum);
		System.out.println("Product Of Even Digits in number = "+product);
	}
}
