/*
1. 8.Write a program to print the product of odd digits in a given number.
Input: 256985
Output:
Product of odd digits: 225
*/

class OddProduct{

	public static void main(String n[]){
	
		int num = 256985;

		System.out.print("Product Of Odd Digits in Number "+num+" is :");

		int product = 1;
		while(num>0){
		
			int rem = num%10;

			if(rem%2==1){
			
				product*=rem;
			
			}
			num/=10;
		}
		System.out.print(product+"\n");
	}
}
