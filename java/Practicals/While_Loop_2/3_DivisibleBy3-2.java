/*
1. 3.Write a program to print the digits from the given number which is divisible by 2 or 3

Input: 436780521

Output: digits divisible by 2 or 3 are: 28634
*/

class TwoThree{

	public static void main (String n[]){
	
		int num =  436780521;

		System.out.print("Digits in given number Divisible by 2 & 3 : ");

		while(num>0){
		
			int rem = num%10;

			if(rem%2==0 || rem%3==0){
				
				System.out.print(rem +" ");
			}
			num/=10;
		}
		System.out.println();
	}
}
