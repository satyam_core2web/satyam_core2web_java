/*
5. Write a program to check whether the given number is divisible by 7 or not.
Input : 105
Output: Divisible by 7
Input: -31
Output: Not divisible by 7
*/

class Divisible{
	
	public static void main(String n[]){
	
		//int num = 105;
		//int num = -31;
		int num = 15;

		if(num%7==0){
		
			System.out.println(num +" is Divisible by 7.");
		} else {

                        System.out.println(num +" is Not Divisible by 7.");
                }
	}
}
