/*
4. Write a program to check whether the given Character is in UPPERCASE (Capital) or
in lowercase. (take hardcoded values)
Input: ch = ‘a’;
Output: a is a lowercase character
Input: ch = ‘A’;
Output: A is an UPPERCASE character
*/

class Character{

	public static void main(String n[]){
	
		//char ch = 'a';
		char ch = 'A';

		if(ch<=90){

			System.out.println(ch +" is a UPPERCASE Character.");

		} else {
		
			System.out.println(ch +" is a lowercase Character.");
		}
	}
}
