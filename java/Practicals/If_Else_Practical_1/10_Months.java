/*
10. Write a program to print the month name according to the month number.
Input : 1
Output: January

Input: 2
Output: February
Input: 13
Output: Invalid input for month
*/

class Month {

	public static void main(String n[]){
		
		System.out.println("Month -\n");
		int num = 22;

		switch(num){

			case 1:
				System.out.println(num +" - January.");
				break;

			case 2:
                                System.out.println(num +" - February.");
                                break;

			case 3:
                                System.out.println(num +" - March.");
                                break;
			
			case 4:
                                System.out.println(num +" - April.");
                                break;

			case 5:
                                System.out.println(num +" - May.");
                                break;

			case 6:
                                System.out.println(num +" - June.");
                                break;

			case 7:
                                System.out.println(num +" - July.");
                                break;

			case 8:
                                System.out.println(num +" - August.");
                                break;

			case 9:
                                System.out.println(num +" - September.");
                                break;

			case 10:
                                System.out.println(num +" - October.");
                                break;

			case 11:
                                System.out.println(num +" - November.");
                                break;

			case 12:
                                System.out.println(num +" - December.");
                                break;

			default : 
				System.out.println(num +" - Out Of Range, Month number must be between 1 to 12!!!!");
		
		
		}
	}
}
