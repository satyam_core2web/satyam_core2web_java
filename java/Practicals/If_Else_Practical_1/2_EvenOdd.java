/*
2. Write a program to check whether the given number is even or odd
Input: num =13;
Output: 13 is an odd number
Input: num =8;
Output: 8 is an even number

*/

class EvenOdd{

	public static void main(String n[]){
	
		int num = 13;
		//int num = 8;
		
		if(num%2==0){
		
			System.out.println(num +" is an Even Number.");
		} else {

                        System.out.println(num +" is an Odd Number.");
                }
	}
}
