/*
3.Write a program to check whether the given character is a vowel or consonant.(take hard
coded values)
Input 1: ‘a’
Output: a is vowel
Input 2: ‘D’
Output: D is consonant
*/

class Character {

	public static void main(String n[]){
	
		//char ch = 'a';
		char ch = 'D';
		//char ch = 'A';
		//char ch = 'z';

		if (ch=='a' || ch=='e' || ch=='o' || ch=='i' || ch=='u'|| ch=='A' || ch=='E' || ch=='O' || ch=='I' || ch=='U'){
		
			System.out.println(ch +" is Vowel.");
		} else {

                        System.out.println(ch +" is Consonant.");
                }

	}
}
