/*
10. Write a program, where you have to write a real time example.(It should be unique and
thought by your own)


 Cricket Match
*/

class RealTime{

	public static void main(String n[]){
	
		int targetRuns = 444;
		int wickets = 8;
		int runs = 445;

		if (wickets < 10){
		
			if(targetRuns > runs ){
			
				System.out.println("Match On, Need '"+(targetRuns - runs)+"' Runs to Win.");
			} else if(targetRuns < runs){
			
				System.out.println("Win The Match by : " +(11-wickets));
			} else {
			
				System.out.println("Loss The Match by " +(targetRuns - runs));
			}
		} else {
		
			System.out.println("All Outs..!");
		}

	}
}
