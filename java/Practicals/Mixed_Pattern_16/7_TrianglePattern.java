/*
 * Q7 Write a program to print the factorial of the number.
Rows = 4
2  4   6  8
10 12 14
16 18
20
Rows = 5
2  4  6  8  10
12 14 16 18
20 22 24
26 28
30
*/

import java.util.*;

class TrianglePattern7{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();


		int num = 2;
		for(int i=1; i<=rows; i++){
	

			for(int j=1; j<=rows-i+1; j++){
			
				if(num%2==0){
					System.out.print(num + "\t");
				
				}

				num+=2;
			}

			System.out.println("\n");
		}
	}
}
