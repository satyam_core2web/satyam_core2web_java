/*
 * Q2 Write a program to print the following pattern
Number of rows = 3
C3 C2 C1
C4 C3 C2
C5 C4 C3
Number of rows = 4
D4 D3 D2 D1
D5 D4 D3 D2
D6 D5 D4 D3
D7 D6 D5 D4
*/

import java.util.*;

class SquareMixPattern{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();

		int ch = 64+rows;
		int num = rows;
		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				System.out.print((char)ch +""+num + "\t");
				num--;
			}
			num = rows + i;
			System.out.println("\n");
		}
	}
}
