
/*
 * Q5 Write a program to check whether the given number is composite or
not.
Row = 3
1
2 4
3 6 9
Rows = 4
1
2 4
3 6 9
4 8 12 16
*/

import java.util.*;

class TableTriangle{


	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){
		
			int num = i;
			for(int j=1; j<=i; j++){
			
				System.out.print(num*j +"\t");
			}
			System.out.println("\n");
		}

	}
}
