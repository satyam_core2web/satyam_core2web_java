/*
 * Q10 Write a program to print the square of odd digits from the given
number. (First reverse this number and then perform the operation)
Input : 45632985632
Output : 25, 9, 81, 25, 9
*/

import java.util.*;

class OddSquare{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int org = num;
		int rem=0,reverse=0;
		while(num>0){
		
			rem=num%10;
			
			reverse= reverse*10+rem;
			num/=10;
		}

		int r = 0;
		while(reverse>0){
		
			r = reverse%10;
			if(r%2==1){

                                System.out.print(r*r +", ");
                        }
			reverse /=10;
		}
	}
}
