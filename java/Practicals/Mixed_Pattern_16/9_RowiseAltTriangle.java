/*
 * Q9 Write a program to reverse the given number.
Rows = 4
1 2 3 4
C B A
1 2
A
Rows = 5
1 2 3 4 5
D C B A
1 2 3
B A
1
*/

import java.io.*;

class RowiweTriangle{

	public static void main (String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
		
			int num = 1;
			int ch =64+ (rows-i+1);
			for(int j=1; j<=rows-i+1;j++){
				
				if(i%2==0){
				
					System.out.print((char)ch +"\t");
					ch--;
				}else{
				
					System.out.print(num +"\t");
					num++;
				}

			}
			System.out.println("\n");
		}
	}
}
