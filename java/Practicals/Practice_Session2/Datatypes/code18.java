class FloatDemo{
	public static void main(String[] args){
		double d1 = 90.89D;
		System.out.println(d1);
		
		float d2 = d1;
		System.out.println(d2);
	}
}
/* output-->

   nikhilbare04@Nikhil-Bare-04:~$ javac code18.java
code18.java:6: error: incompatible types: possible lossy conversion from double to float
                float d2 = d1;
                           ^
1 error

*/
