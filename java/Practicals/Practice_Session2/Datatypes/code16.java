class FloatDemo{
	public static void main(String[] args){
		int n1 = 'Z';
		float n2 = n1; 		// n2 =n1 output -->90.0
   		float n2 = n1;		// error- two variable name is same
		System.out.println(n2);
	}
}


/* output-->
 
nikhilbare04@Nikhil-Bare-04:~$ javac code16.java
code16.java:5: error: variable n2 is already defined in method main(String[])
                float n2 = n1;
                      ^
1 error

 
 */
