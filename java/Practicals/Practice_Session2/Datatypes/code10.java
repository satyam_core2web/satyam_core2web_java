class Boolean{
	public static void main(String[] args){
		boolean value1 = 1;		// 0 and 1 is integer value hence error
		boolean value2 = 0;
		System.out.println(value1);
		System.out.println(value2);
	}
}
/* output-->
 
nikhilbare04@Nikhil-Bare-04:/mnt/e/java/Practicals/Datatypes$ javac code10.java
code10.java:3: error: incompatible types: int cannot be converted to boolean
                boolean value1 = 1;
                                 ^
code10.java:4: error: incompatible types: int cannot be converted to boolean
                boolean value2 = 0;
                                 ^
2 errors 
 
 */
