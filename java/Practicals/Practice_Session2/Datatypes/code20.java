class FloatDemo{
	public static void main(String [] args){
		char ch = 'A';
		int n1 = ch;
		float f1 = n1;
		double d1 = f1;

		System.out.println(n1);
		System.out.println(f1);
		System.out.println(d1);
	
	}
}

//output-->
//65
//65.0
//65.0


