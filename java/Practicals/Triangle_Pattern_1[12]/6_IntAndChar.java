/*
6. Write a program to print the given pattern
rows=3
1
B C
1 2 3

rows=4
1
B C
1 2 3
G H I J
*/

import java.io.*;

class IntAndChar{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. Rows : ");
		int rows = Integer.parseInt(br.readLine());

		char ch = 'A';

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=i; j++){
			
				if(i%2==0){
				
					System.out.print((char) ch+"  ");
				} else {
				
					System.out.print(j +"  ");
				}
				ch++;
			}
			System.out.println("\n");
		}
	}
}
