/*
5. Write a program to print the given pattern
rows =3

D

E F
G H I

rows = 4
E
F G
H I J
K L M N
*/

import java.io.*;

class CharIncrement{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int ch = 65+rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=i; j++){
			
				System.out.print((char)ch +"  ");
				ch++;
			}
			System.out.println("\n");
		}
	}
}
