/*
 2.Write a program to print the given pattern
rows=3
a
$ $
a b c

rows=4
a
$ $
a b c
$ $ $ $
*/

import java.io.*;

class CharAndSymbol{

	public static void main(String []args)throws IOException{
	
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			char ch = 'a';

			for(int j=1; j<=i; j++){
			
				if(i%2==1){
				
					System.out.print(ch +"  ");
					ch++;
				} else {
				
					System.out.print("$  ");
				}

			}
			System.out.println("\n");
		}
	}
}
