/*
4.Write a program to print the given pattern
rows=3
c
C B
c b a
rows=4
d
D C
d c b
D C B A
*/

import java.io.*;

class BothChar{
	
	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			int capital = 64+rows;
			int small = 96+rows;

			for(int j=1; j<=i; j++){
			
				if(i%2==0){
				
					System.out.print((char)capital +"  ");
				} else {
				
					System.out.print((char)small +"  ");
				}
				capital--;
				small--;
			}
			System.out.println("\n");
		}
	}
}
