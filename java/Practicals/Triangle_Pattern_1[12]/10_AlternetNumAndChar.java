/*
10. Write a program to print the given pattern
rows=3
1
b c
4 5 6

rows=4
1
b c
4 5 6
g h i j
*/

import java.io.*;

class Alternet{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = 1;
		char ch = 'a';
		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=i; j++){
			
				if(i%2==0){
				
					System.out.print(ch +"   ");
				} else {
				
					System.out.print(num +"   ");
				}
				num++;
				ch++;
			}
			System.out.println("\n");
		}
	}
}
