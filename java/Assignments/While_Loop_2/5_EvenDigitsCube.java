/*
1. 5.Write a program to print the cubes of even digits of a given number.
Input: 216985
Output: 512 216 8

*/


class EvenCube{

	public static void main(String n[]){
	
		int num = 216985;

		System.out.print("Cube of Even Digits in " +num +" is : ");
		while(num>0){
		
			int rem = num%10;

			if(rem%2==0){
			
				System.out.print(rem*rem*rem + "   ");
			}
			num/=10;

		}
		System.out.println();
	}
}
