/*
1. 6.Write a program to print the product of digits in a given number
Input: 234
Output: 24
Explanation: 2*3*4
*/

class Product{

	public static void main(String n[]){
	
		int num = 234;
		int product = 1;

		System.out.print("Product of Digits in " +num +" is : ");
		
		while(num>0){
		
			int rem = num%10;
			product *= rem;

			System.out.print(rem+" * ");

			num/=10;

		}
		System.out.print(" = "+product+"\n");
	}
}
