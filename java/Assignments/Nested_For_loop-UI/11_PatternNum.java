/*
Q10 Write a program to print the following pattern
Number of rows = 4
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7

Number of rows = 3
1 2 3
2 3 4
3 4 5
*/

import java.util.*;

class PatternNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Number of Row & Column : ");
		int row = sc.nextInt();

		int num =1;

		for(int i=1; i<=row; i++){

			for(int j=1; j<=row; j++){
			
				System.out.print(num++ +"   ");
			}
			num-=row-1;
			System.out.println("\n");
		}
	}
}
