/*
Q4 Write a program to print the following pattern
table formate
*/



import  java.util.*;

class Tables{

        public static void main(String n[]){

                Scanner sc = new Scanner(System.in);
                System.out.print("how Many Tables : ");
                int col = sc.nextInt();
		

                for (int i = 1; i<=10; i++){

			int num = 1;

                        for(int j=1; j<=col; j++){

                                System.out.print(i*num +"    ");
				num++;
                        }

                        System.out.println("\n");
                }
        }
}
