/*
Q6 Write a program to print the following pattern
Number of rows = 3
1A 1A 1A
1A 1A 1A
1A 1A 1A
Number of rows = 4
1A 1A 1A 1A
1A 1A 1A 1A
1A 1A 1A 1A
1A 1A 1A 1A
*/


import java.util.*;

class CharNumPattern{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("How Many Row & Column : ");
		int row = sc.nextInt();

		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
			
				System.out.print(" 1A  ");
				
			}

			System.out.println("\n");
		}
	}
}
