/*
 2. A person is in the bank and wants to print the bank balance (Write a
code to print the bank balance of the person), also wants to print the
bank address.
*/

class Bank{

	public static void main(String n[]){
	
		float balance = 444333.534f;
		String branch = "Yeola";

		System.out.println("Bank Balance - " +balance);
		System.out.println("Branch - " +branch);
	}
}
