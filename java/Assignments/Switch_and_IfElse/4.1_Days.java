/*
Q4.
Write a program to check the day number(1-7) and print the corresponding day of
week.
Input : 5
Output : Friday
Input : 7
Output : Sunday
*/

class Days{

	public static void main(String n[]){
	
		int dayNum = 4;

		if(dayNum == 1){
		
			System.out.println(dayNum +". Monday");
		} else if(dayNum == 2){

                        System.out.println(dayNum +". Tuesday");
                } else if(dayNum == 3){

                        System.out.println(dayNum +". Wednesday");
                } else if(dayNum == 4){

                        System.out.println(dayNum +". Thursday");
                } else if(dayNum == 5){

                        System.out.println(dayNum +". Friday");
                } else if(dayNum == 6){

                        System.out.println(dayNum +". Saturday");
                } else if(dayNum == 7){

                        System.out.println(dayNum +". Sunday");
                } else {
		
			System.out.println("Please Enter Valid Input between 1 to 7.");
		}
	} 
}
