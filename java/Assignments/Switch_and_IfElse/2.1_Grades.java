/*
 Q2.
Write a program to print Remarks according to their respective grades.
Input : O
Output : Outstanding
Input : A
Output : Excellent
   */

class Grades{

	public static void main (String n[]){
	
		//char ch = 'O';
		char ch = 'P';

		if (ch == 'O'){

			System.out.println("Outstanding.");
		
		} else if(ch == 'A'){
		
			System.out.println("Excellent.");
	
		} else if(ch == 'B'){

                        System.out.println("Very Good.");
               
	       	} else if(ch == 'C'){

                        System.out.println("Good.");
                
		} else if(ch == 'P'){

                        System.out.println("Pass.");
                
		} else {
		
			System.out.println("Fail.");
		}
	}
}
