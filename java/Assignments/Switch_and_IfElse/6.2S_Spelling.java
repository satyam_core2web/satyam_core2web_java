/*
Q6.switch
Write a program that takes a number from 0 to 5 and print its spelling, if the
entered number is greater than 5 print entered number is greater than 5.
Input : 3
Output : Three
Input : 6
Output : 6 is greater than 5.
*/

class NumberS{

	public static void main(String n[]){
	
		int num = 4;

		switch (num){
		
			case 1:
				System.out.println(num +"- ONE");
				break;

			case 2:
                                System.out.println(num +"- TWO");
                                break;

			case 3:
                                System.out.println(num +"- THREE");
                                break;

			case 4:
                                System.out.println(num +"- FORE");
                                break;

			case 5:
                                System.out.println(num +"- FIVE");
                                break;

			default : 
				System.out.println(num +"- is Greater than 5.");
		}
	}
}
