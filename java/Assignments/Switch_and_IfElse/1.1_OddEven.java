/*
 Q1.If Else
Write a program to check whether the given number is odd or even.
Input : 45
Output : 45 is an odd number. 
 */

class OddEven1{

	public static void main(String n[]){
	
		int num  = 45;

		if (num%2==0){
			
			System.out.println(num +" is an Even Number.");
		} else {
		
			System.out.println(num +" is an Odd Number.");
		}

	}
}
