/*
Q3.
Write a program to print longform of cloth sizes according to their acronym.
Input : S
Output : Small
Input : XL
Output : Extra Large
*/

class ClothSize{

	public static void main(String n[]){
	
		String size = "XL";

		if(size == "S"){
		
			System.out.println("Small.");	
		} else if(size == "M") {
		
			System.out.println("Medium.");
		} else if(size == "L") {

                        System.out.println("Large.");
                } else if(size == "XL") {

                        System.out.println("Extra Large.");
                } else if(size == "XXL"){
		
			System.out.println("Extra-Extra Large.");
		} else {
		
			System.out.println("This Size Can not Find.");
		}
	}
}
