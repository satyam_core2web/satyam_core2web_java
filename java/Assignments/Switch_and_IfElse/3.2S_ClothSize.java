/* Swith
Q3.
Write a program to print longform of cloth sizes according to their acronym.
Input : S
Output : Small
Input : XL
Output : Extra Large
*/

class ClothSizeS{

	public static void main(String n[]){
	
		String size = "M";

		switch(size){
		
			case "S":
				System.out.println("Small.");
				break;
			case "M":
                                System.out.println("Medium.");
                                break;

			case "L":
                                System.out.println("Large.");
                                break;

			case "Xl":
                                System.out.println("SExtra Large.");
                                break;

			case "XXL":
                                System.out.println("Extra-Extra Large.");
                                break;
			default:
				System.out.println("This Size can not Found.");
		}
	}
}
