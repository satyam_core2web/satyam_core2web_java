/*If Else
 Q5.
Write a program to print the month name according to the month number.
Input : 4
Output : April
Input : 9
Output : September
*/

class MonthsS{

	public static void main(String n[]){
		
		int monthNum = 12;

		switch(monthNum){
		
			case 1:
				System.out.println(monthNum +"-> January");
				break;

			case 2:
                                System.out.println(monthNum +"-> February");
                                break;

			case 3:
                                System.out.println(monthNum +"-> march");
                                break;

			case 4:
                                System.out.println(monthNum +"-> April");
                                break;

			case 5:
                                System.out.println(monthNum +"-> May");
                                break;

			case 6:
                                System.out.println(monthNum +"-> June");
                                break;

			case 7:
                                System.out.println(monthNum +"-> July");
                                break;

			case 8:
                                System.out.println(monthNum +"-> August");
                                break;

			case 9:
                                System.out.println(monthNum +"-> September");
                                break;

			case 10:
                                System.out.println(monthNum +"-> Octomber");
                                break;

			case 11:
                                System.out.println(monthNum +"-> November");
                                break;

			case 12:
                                System.out.println(monthNum +"-> December");
                                break;

			default :
				System.out.println("Enter the valid Input of Month Number between 1- 12 .......");
		}

	}
}
