/*
Q4.
Write a program to check the day number(1-7) and print the corresponding day of
week.
Input : 5
Output : Friday
Input : 7
Output : Sunday

*/

class DaysS{

	public static void main(String n[]){
	
		int dayNum = 8;

		switch (dayNum){
		
			case 1:
				System.out.println(dayNum +". Monday");
				break;

			case 2:
                                System.out.println(dayNum +". Tuesday");
                                break;

			case 3:
                                System.out.println(dayNum +". Wednesday");
                                break;

			case 4:
                                System.out.println(dayNum +". Thursday");
                                break;

			case 5:
                                System.out.println(dayNum +". Friday");
                                break;

			case 6:
                                System.out.println(dayNum +". Saturday");
                                break;

			case 7:
                                System.out.println(dayNum +". Sunday");
                                break;

			default :
				System.out.println("Please Enter the Valid Input between 1 to 7");
		}


	}
} 
