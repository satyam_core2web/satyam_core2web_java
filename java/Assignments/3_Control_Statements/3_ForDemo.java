/*
3. Dry run the given code in your notebook where you have to check is there any error if
yes mention so also if there is no error write the output.
*/

class ForDemo {

	public static void main(String n[]){
	
		int num = 69;

		for(;num<=90;){
		
			char ch = 70;
			
			ch+=num;
			
			System.out.println(ch);
			num++;
		}
	}
}
