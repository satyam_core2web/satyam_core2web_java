/*
12. Write a program where you need to use the concepts you learned in
operators topic.
 */
//--->
// Arithmetic = +,-,/,*,%,
// Unary = ++,--,+,-
// Assignment = =,+=,-=,*=,/=,%=
// Relational = ==,!=,<,>,<=,>=
// Logical = !,&&,||
// Bitwise = &,|,^,>>,<<,~,>>>

class AllOperators{
	public static void main (String n[]){
		int n1 = 8;
		int n2 = 12;
		
		//Arithmetic Operations -
		
		System.out.println("Arithmetic Operations --\n");
		System.out.println("Addoition (+) = "+ (n1+n2));
		System.out.println("Subtraction (-) = "+ (n1-n2));
		System.out.println("Multiplication (*) = "+ (n1*n2));
		System.out.println("Division (/) = "+ (n1/n2));

		
		System.out.println("Unary Operation --\n");
		System.out.println("Addoition (+) = "+ (n1+ ++n2) +"\n");	// ..... = 21
		
		
		System.out.println("Relational Operations --\n");
		System.out.println(n1 +" == " +n2 +" = "+(n1 == n2));
		System.out.println(n1 +" != " +n2 +" = "+(n1 != n2));
		System.out.println(n1 +" > " +n2 +" = "+(n1 > n2));
		System.out.println(n1 +" == " +n2 +" = "+(n1 >= n2));
		System.out.println(n1 +" == " +n2 +" = "+(n1 < n2));
		System.out.println(n1 +" == " +n2 +" = "+(n1 <= n2));

		/*
		System.out.println("Logical Operations --\n");
		System.out.println(" ! " +n1 +" = "+(!n1));
		System.out.println(n1 +" && " +n2 +" = "+(n1 && n2));
		System.out.println(n1 +" || " +n2 +" = "+(n1 || n2));

		*/

		System.out.println("Bitwise Operations --\n");
		System.out.println(n1 +" & " +n2 +" = "+(n1 & n2));
		System.out.println(n1 +" | " +n2 +" = "+(n1 | n2));
		System.out.println(n1 +" ^ " +n2 +" = "+(n1 ^ n2));
		System.out.println(n1 +" >>> 2 = "+(n2 >>> 2));
	}
}
