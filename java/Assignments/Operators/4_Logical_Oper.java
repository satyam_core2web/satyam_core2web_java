// &&,||,!

class LogicalOperations{
	public static void main(String n[]){
		boolean val1 = true;
		boolean val2 = false;


		System.out.println("Value_1 = "+val1 + ",\nvalue_2 = " + val2 +"\n");
		
		System.out.println("Value_1 && value_2 - "+(val1 && val2));
		System.out.println("Value_1 || value_2 - "+(val1 || val2));
		System.out.println("!Value_1 - "+(! val1));
		System.out.println("!Value_2 - "+(! val2));
	}
}
