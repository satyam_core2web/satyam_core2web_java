// &,|,^,<<,>>,>>>


class BitwiseOperators{
	public static void main(String n[]){
		int n1 = 5;
		int n2 = 3;

		System.out.println("Number_1 = " + n1 + "\nNumber_2 = " + n2 + "\n");

		System.out.println("Bitwise | -> " + (n1 | n2));
		System.out.println("Bitwise ^ -> " + (n1 ^ n2));
		System.out.println("Left Shift Number_1 by 2 -> " + (n1 << 1));
		System.out.println("Right Shift Number_1 by 2 -> " + (n1 >> 2));
		System.out.println("Unsigned Right Shift Number_1 by 2 -> " + (n1 >>> 2));
	}
}
