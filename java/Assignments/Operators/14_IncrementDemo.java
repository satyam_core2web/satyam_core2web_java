/*
 14. Write an output for the following.
Remember you have to first solve it in your own notebook and then
type the code in your PCs.
If x = 14 and y = 22

1. ++x + y++
2. x++ + ++y + ++x + ++x
3. y++ + ++x + ++x
 
 */

class IncDemo{
	public static void main(String n[]){
		int x = 14;
		int y = 22;
		
		System.out.println("x - " +x);
		System.out.println("y - " +y +"\n");

		int n1,n2,n3;
		n1 = ++x + y++;
		n2 = x++ + ++y + ++x + ++x;
		n3 = y++ + ++x + ++ x;

		System.out.println("++x + y++ = " +n1);
		System.out.println("x++ + ++y + ++x + ++x = " +n2);
		System.out.println("y++ + ++x + ++x = " +n3);
	}
}
