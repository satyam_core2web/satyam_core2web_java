/*
13. Write an output for the following.
Remember you have to first solve it in your own notebook and then
type the code in your PCs.

If x = 19
1. x++ + x++
2. ++x + x++ + ++x 
*/

class IncrementDemo{
	public static void main (String n[]){
		int num = 19;

		int x = num++ + num++;
		int y = ++num + num++ + ++num;

		System.out.println(x);
		System.out.println(y);
	}
}
