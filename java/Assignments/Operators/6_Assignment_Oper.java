// =,+=,-=,*=,/=,%=


class AssignmentOperators{
	public static void main(String n[]){
		int a = 10;
		int b = 5;
		
		System.out.println("a = "+ a + "\nb = " + b +"\n");
		System.out.println("a+=3 -> " +(a+=3));
		System.out.println("b-=2 -> " +(b-=2));
		System.out.println("a*=2 -> " +(a*=2));
		System.out.println("b/=3 -> " +(b/=3));
		System.out.println("a%=5 -> " +(a%=5));
	}
}
