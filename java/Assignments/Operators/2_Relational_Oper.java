// ==,!=, <,<=, >, >=


class RelationalOperations{
	public static void main(String n[]){
		int n1 = 10;
		int n2 = 4;
		

		System.out.println("Number 1 -- "+n1);
		System.out.println("Number 2 -- "+n2);
		System.out.println("-----------------------------------------");
		
		System.out.println("Number 1 == Number 2 --> "+(n1==n2));
		System.out.println("Number 1 != Number 2 --> "+(n1!=n2));
		System.out.println("Number 1 > Number 2 --> "+(n1>n2));
		System.out.println("Number 1 >= Number 2 --> "+(n1>=n2));
		System.out.println("Number 1 < Number 2 --> "+(n1<n2));
		System.out.println("Number 1 <= Number 2 --> "+(n1<=n2));
	}
}
