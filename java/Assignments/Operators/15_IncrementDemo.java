/*
 15. Write a real time example which uses 5 different operators.

If x = 0
1. ++x + ++x + ++x + ++x
2. x++ + x++ + x++ + x++

 * */

class Increments{
	public static void main(String args[]){
		int n = 0;
		
		System.out.println("n - " +n);

		int x = ++n + ++n + ++n + ++n;
		int y = n++ + n++ + n++ + n++;
		
		System.out.println("++n + ++n + ++n + ++n = " +x);
		System.out.println("n++ + n++ + n++ + n++ = " +y);
	}
}
