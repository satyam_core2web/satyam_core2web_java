/*
Q5 Write a program to take a number from the user and check whether the number is present in the table of 16 or not.

Input 1: num = 32
Output: 32 is present in the table of 16.
Input 2: num =45:
Output: 45 is not present in the table of 16.
*/

import java.util.*;

class Table16{

	public static void main(String n[]){
	
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		if(num%16==0){
		
			System.out.println(num +" is Present in the Table of 16.");
		} else {
		
			 System.out.println(num +" is Not Present in the Table of 16.");
		}
	}
}
