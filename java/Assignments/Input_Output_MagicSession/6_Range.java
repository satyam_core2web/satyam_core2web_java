/*
Q6 Write a program to take range as input from the user and print the numbers between them.

Input 1: num1 = 5
num * 2 = 16
Output: 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
Input 2: num1 = - 2
num * 2 = 4
Output: -2,-1,0,1,2,3,4,
*/

import java.util.*;

class Range{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter 1st Number : ");
		int num1 = sc.nextInt();

		System.out.print("Enter 2nd Number : ");
		int num2 = sc.nextInt();

		for(int i=num1; i<=num2 ; i++){
		
			System.out.print(i +"  ");
		}
		System.out.print("\n ");
	}
}
