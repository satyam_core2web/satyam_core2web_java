/*
Q8 Write a program to take range as input from the user and print th sum of the numbers in the range.

Input 1: numl= 5
num * 2 = 7
Output: 18
Input 2: numl= - 2
num * 2 = 4
Output: 7
*/

import java.util.*;

class Sum{

	public static void main(String n[]){
		
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter 1st Number : ");
		int num1 = sc.nextInt();

		System.out.print("Enter 2nd Number : ");
		int num2 = sc.nextInt();

		int sum =0;

		for(int i=num1; i<=num2; i++){
		
			sum+=i;
		}
		System.out.println("Sum = "+sum);

	}

}
