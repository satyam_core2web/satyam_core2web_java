/*
Q7 Write a program to take a number as input from the user and print the reverse table of it.
Input 1: num = 9;
Output: 90, 81, 72, 63, 54, 45, 36, 27, 18, 9,
*/

import java.util.*;

class ReverseTable{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number for Table : ");
		int num  = sc.nextInt();

		int i = 10;

		while(i>0){
		
			System.out.print(num*i +",  ");
			i--;
		}

		System.out.println();
	}
}
