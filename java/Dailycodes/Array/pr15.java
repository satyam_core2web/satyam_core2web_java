import java.util.*;

class OneDArray{

	public static void main(String[] args){

		int empId = new int[3];

		empId[0] = 10;
		
		empId[1] = 20;
		
		empId[2] = 30;

		System.out.println(empId[0]);
		
		System.out.println(empId[1]);
		
		System.out.println(empId[2]);

	}

}

 /* pr15.java:7: error: incompatible types: int[] cannot be converted to int
                int empId = new int[3];
                            ^
pr15.java:9: error: array required, but int found
                empId[0] = 10;
                     ^
pr15.java:11: error: array required, but int found
                empId[1] = 20;
                     ^
pr15.java:13: error: array required, but int found
                empId[2] = 30;
                     ^
pr15.java:15: error: array required, but int found
                System.out.println(empId[0]);
                                        ^
pr15.java:17: error: array required, but int found
                System.out.println(empId[1]);
                                        ^
pr15.java:19: error: array required, but int found
                System.out.println(empId[2]); /*
                                        ^
7 errors
